const shared = {
  type: 'line',
};

export const timeAxis = {
  xAxes: [
    {
      type: 'time',
      display: true,
      ticks: {
        autoSkip: true,
      },

      time: {
        // unit: 'week',
      },
    },
  ],
};

const sharedOptions = {
  pan: {
    enabled: true,
    mode: 'xy',
    speed: 20,
    threshold: 10,
  },
  zoom: {
    enabled: true,
    mode: 'x',
    limits: {
      max: 10,
      min: 0.5,
    },
  },
  responsive: true,
  title: {
    display: false,
  },
  legend: {
    display: false,
  },
  tooltips: {
    position: 'nearest',
    mode: 'index',
    intersect: false,
    displayColors: true,
    callbacks: {
      label: function(tooltipItem, ref) {
        var dataset = ref.datasets[tooltipItem.datasetIndex];
        var item = dataset.data[tooltipItem.index];
        if (dataset.formatLabel) {
          return dataset.formatLabel(item);
        }
        return '';
      },
      afterBody: function(highlight, ref) {
        var tooltipItem = highlight[0];
        var dataset = ref.datasets[tooltipItem.datasetIndex];
        var item = dataset.data[tooltipItem.index].data;

        return item.afterBody ? item.afterBody : [];
      },
    },
  },
};

const eslint = dataset => {
  return {
    ...shared,
    data: {
      datasets: [
        {
          borderColor: '#2ECC40',
          backgroundColor: '#2ECC40',
          fill: false,
          pointRadius: 0,
          data: dataset.percentage,
          yAxisID: 'y-axis-1',
          label: 'percentage',
          formatLabel: item => 'Polished files: ' + item.y + '%',
        },
        {
          borderColor: '#ff4136',
          backgroundColor: '#ff4136',
          fill: false,
          pointRadius: 0,
          data: dataset.errors,
          yAxisID: 'y-axis-2',
          label: 'errors',
          formatLabel: item => 'Errors: ' + item.y,
        },
        {
          borderColor: '#ccb42d',
          backgroundColor: '#ccb42d',
          fill: false,
          pointRadius: 0,
          data: dataset.warnings,
          yAxisID: 'y-axis-2',
          label: 'warnings',
          formatLabel: item => 'Warnings: ' + item.y,
        },
      ],
    },
    options: {
      ...sharedOptions,
      scales: {
        ...timeAxis,
        yAxes: [
          {
            display: true,
            position: 'left',
            id: 'y-axis-1',
            scaleLabel: {
              display: true,
              labelString: 'Polished files in %',
            },
            ticks: {
              suggestedMin: 75,
              suggestedMax: 100,
            },
          },
          {
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            ticks: {
              suggestedMin: 0,
              suggestedMax: 5000,
            },
            scaleLabel: {
              display: true,
              labelString: 'Amount of Errors/Warnings',
            },
            // grid line settings
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  };
};

const prettier = dataset => {
  return {
    ...shared,
    data: {
      datasets: [
        {
          borderColor: '#2ECC40',
          backgroundColor: '#2ECC40',
          fill: false,
          pointRadius: 0,
          data: dataset.percentage,
          yAxisID: 'y-axis-1',
          label: 'percentage',
          formatLabel: item => 'Prettiness: ' + item.y + '%',
        },
        {
          borderColor: '#2D8FCC',
          backgroundColor: '#2D8FCC',
          fill: false,
          pointRadius: 0,
          data: dataset.totalLines,
          yAxisID: 'y-axis-2',
          label: 'totalLines',
          formatLabel: item => 'Total line changes: ' + item.y,
        },
      ],
    },
    options: {
      ...sharedOptions,
      scales: {
        ...timeAxis,
        yAxes: [
          {
            display: true,
            position: 'left',
            id: 'y-axis-1',
            scaleLabel: {
              display: true,
              labelString: 'Prettiness in %',
            },
            ticks: {
              suggestedMin: 25,
              suggestedMax: 100,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
          {
            type: 'logarithmic',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            ticks: {
              min: 0,
              max: 100000,
              callback: function(tick) {
                return formatLogTick(tick);
              },
            },
            scaleLabel: {
              display: true,
              labelString: 'Total line changes needed',
            },
          },
        ],
      },
    },
  };
};

const jest = dataset => {
  return {
    ...shared,
    data: {
      datasets: [
        {
          borderColor: '#008000',
          backgroundColor: 'rgba(46,204,64, 0.6)',
          borderWidth: 1,
          fill: 'start',
          pointRadius: 0,
          data: dataset.jest,
          yAxisID: 'y-axis-1',
          label: 'percentage',
          formatLabel: item => `Jest: ${item.y} `,
        },
        {
          borderColor: '#004380',
          backgroundColor: 'rgba(45,143,204, 0.6)',
          borderWidth: 1,
          fill: '-1',
          pointRadius: 0,
          data: dataset.karma,
          yAxisID: 'y-axis-1',
          label: 'totalLines',
          formatLabel: item => `Karma: ${item.y}`,
        },
      ],
    },
    options: {
      ...sharedOptions,
      scales: {
        ...timeAxis,
        yAxes: [
          {
            stacked: true,
            display: true,
            position: 'left',
            id: 'y-axis-1',
            scaleLabel: {
              display: true,
              labelString: 'File count',
            },
            ticks: {
              suggestedMin: 0,
              suggestedMax: 100,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  };
};

const formatLogTick = tick => {
  if (tick === 0) {
    return 0;
  }

  const power = Math.log10(tick);

  const multiplier = tick / 10 ** Math.floor(power);

  if (multiplier > 1 && multiplier % 2 === 1) {
    return undefined;
  }

  const prefix = multiplier > 1 ? `${multiplier}x` : '';

  let pow = Math.floor(power)
    .toString()
    .split('')
    .map(char => {
      const digit = parseInt(char, 10);
      switch (digit) {
        case 1:
          return 0x00b9;
        case 2:
        case 3:
          return 0x00b0 + digit;
        default:
          return 0x2070 + digit;
      }
    });

  pow = String.fromCharCode(...pow);

  return `${prefix}10${pow}`;
};

const diff = dataset => {
  return {
    ...shared,
    data: {
      datasets: [
        {
          borderColor: '#2D8FCC',
          backgroundColor: '#2D8FCC',
          fill: false,
          pointRadius: 0,
          data: dataset.diffFiles,
          yAxisID: 'y-axis-1',
          label: 'diffFiles',
          formatLabel: item => 'Touched Files: ' + item.y,
        },
        {
          borderColor: '#2ECC40',
          backgroundColor: '#2ECC40',
          fill: false,
          pointRadius: 0,
          data: dataset.additions,
          yAxisID: 'y-axis-2',
          label: 'additions',
          formatLabel: item => 'Insertions: ' + item.y,
        },
        {
          borderColor: '#ff4136',
          backgroundColor: '#ff4136',
          fill: false,
          pointRadius: 0,
          data: dataset.deletions,
          yAxisID: 'y-axis-2',
          label: 'deletions',
          formatLabel: item => 'Deletions: ' + item.y,
        },
      ],
    },
    options: {
      ...sharedOptions,
      scales: {
        ...timeAxis,
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-1',
            scaleLabel: {
              display: true,
              labelString: 'Touched files',
            },
            ticks: {
              min: 0,
              max: 3500,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
          {
            type: 'logarithmic',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            scaleLabel: {
              display: true,
              labelString: 'Deletions/insertions',
            },
            ticks: {
              min: 0,
              max: 120000,
              autoSkip: true,
              callback: function(tick) {
                return formatLogTick(tick);
              },
            },
            // grid line settings
            gridLines: {
              drawOnChartArea: true,
            },
          },
        ],
      },
    },
  };
};

const getChartConfig = (target, dataset) => {
  switch (target) {
    case 'prettier':
      return prettier(dataset);
    case 'eslint':
      return eslint(dataset);
    case 'diff':
      return diff(dataset);
    case 'jest':
      return jest(dataset);
    default:
      throw new Error(`getChartConfig: Unknown target ${target}`);
  }
};

export default getChartConfig;
