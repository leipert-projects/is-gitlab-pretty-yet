module.exports = function(results) {
  const slimmed = results.map(x => {
    delete x.source;
    x.messages = x.messages.map(m => {
      delete m.source;
      delete m.fix;
      return m;
    });
    return x;
  });

  return JSON.stringify(slimmed);
};
